require 'spec_helper'

describe 'zabbix::database' do
    context 'When all database environments for zabbix are set on RHEL/CentOS' do
        let(:chef_run) do
            ChefSpec::SoloRunner.new do |node|
                # Create a new environment (you could also use a different :let block or :before block)
                env = Chef::Environment.new
                env.name 'unit_test'

                # Stub the node to return this environment
                allow(node).to receive(:chef_environment).and_return(env.name)

                # Stub any calls to Environment.load to return this environment
                allow(Chef::Environment).to receive(:load).and_return(env)

                # Stubbing out fqdn node attribute
                node.automatic['fqdn'] = 'unit.testing.stub'
            end.converge(described_recipe)
        end

        before(:each) do
            # Stubbing Directory results
            allow(Dir).to receive(:exist?).with('/tmp/database').and_return(true)
            allow(Dir).to receive(:empty?).with('/var/lib/pgsql/9.4/data/*').and_return(true)

            allow(Chef::EncryptedDataBagItem).to receive(:load).with('odhp_credentials', 'credentials').and_return(
            {
                'zabbix' => {
                    'users' => {
                        'unit_test_env' => {
                            'postgres' => {
                                'username' => 'postgres_username',
                                'password' => 'postgres_password'
                            }
                        }
                    }
                }
            })
        end

        let(:pg_hba_template) { chef_run.template('/var/lib/pgsql/9.4/data/pg_hba.conf') }

        it 'installs a package postgresql94-server' do
            expect(chef_run).to install_yum_package('postgresql94-server')
        end

        it 'creates a directory database' do
            expect(chef_run).to create_directory('/tmp/database')
        end

        it 'creates schema.sql' do
            expect(chef_run).to create_template('/tmp/database/schema.sql')
            .with(
                source: 'schema.sql.erb',
                mode: '0644',
                owner: 'root',
                group: 'root'
            )
        end

        it 'creates images.sql' do
            expect(chef_run).to create_template('/tmp/database/images.sql')
            .with(
                source: 'images.sql.erb',
                mode: '0644',
                owner: 'root',
                group: 'root'
            )
        end

        it 'creates data.sql' do
            expect(chef_run).to create_template('/tmp/database/data.sql')
            .with(
                source: 'data.sql.erb',
                mode: '0644',
                owner: 'root',
                group: 'root'
            )
        end

        it 'runs the yum command to initialize postgresql db' do
            expect(chef_run).to run_execute('initializingdb')
        end

        it 'creates pg_hba.conf' do
            expect(chef_run).to create_template('/var/lib/pgsql/9.4/data/pg_hba.conf')
            .with(
                source: 'pg_hba.conf.erb',
                mode: '0600',
                owner: 'postgres',
                group: 'postgres'
            )
            expect(pg_hba_template).to notify('service[postgresql-9.4]').to(:restart).immediately
        end

        it 'executes both start and enable actions for postgresql-9.4' do
            expect(chef_run).to start_service('postgresql-9.4')
            expect(chef_run).to enable_service('postgresql-9.4')
        end

        it 'runs configures postgres db' do
            expect(chef_run).to run_bash('configure_postgres')
        end

        it 'runs configure postgres schema' do
            expect(chef_run).to run_bash('configure_schema')
        end

        it 'cleans up temporary files' do
            expect(chef_run).to delete_directory('cleanup')
        end
    end
end
