#
# Cookbook Name:: zabbix
# Spec:: agent
#
# Copyright (c) 2015 Mobiquity Inc, All Rights Reserved.

require 'spec_helper'

describe 'zabbix::agent' do
	context 'When all zabbix tags are enabled, on RHEL/CentOS' do
		let(:chef_run) do
			ChefSpec::SoloRunner.new do |node|
				# Create a new environment (you could also use a different :let block or :before block)
				env = Chef::Environment.new
				env.name 'unit_test_env'

				# Stub the node to return this environment
				allow(node).to receive(:chef_environment).and_return(env.name)

				# Stub any calls to Environment.load to return this environment
				allow(Chef::Environment).to receive(:load).and_return(env)

				# Stubbing out fqdn node attribute
				node.automatic['fqdn'] = 'unit.testing.stub'
                node.normal['tags'] = [ 'httpd', 'openldap', 'mongodb', 'shibboleth', 'tomcat' ]
			end.converge(described_recipe)
		end

		before do
			allow(Chef::EncryptedDataBagItem).to receive(:load).with('odhp_credentials', 'credentials').and_return(
				{
					'mongo' => {
						'users' => {
							'unit_test_env' => {
								'mmsagent' => {
									'username' => 'unit_test_username',
									'password' => 'unit_test_password'
								}
							}
						}
					}
				})
		end

		let(:zabbix_agent_template) { chef_run.template('/etc/zabbix/zabbix_agentd.conf') }

		it 'installs zabbix packages' do
			%w{zabbix-agent zabbix-sender}.each do |pkg|
				expect(chef_run).to install_yum_package(pkg)
				.with(
                    version: '3.0.5-1.el6',
					allow_downgrade: true
				)
			end
		end

		it 'installs wget' do
			%w{wget crontabs cronie}.each do |pkg|
				expect(chef_run).to install_yum_package(pkg)
			end
		end

		it 'generates zabbix_agentd.conf file' do
			expect(chef_run).to create_template('/etc/zabbix/zabbix_agentd.conf')
			.with(
				owner: 'root',
				group: 'root',
				mode: '0644',
				source: 'zabbix_agentd.conf.erb'
			)
			expect(zabbix_agent_template).to notify('service[zabbix-agent]').to(:restart).delayed
		end

		it 'enables and starts zabbix service' do
			expect(chef_run).to enable_service('zabbix-agent')
			expect(chef_run).to start_service('zabbix-agent')
		end

		it 'creates zabbix trap directories' do
			%w[ /etc/zabbix/trap.d /etc/zabbix/trap.d/live /etc/zabbix/trap.d/daily ].each do |path|
				expect(chef_run).to create_directory(path)
				.with(
					owner: 'root',
					group: 'root',
					mode: '0755'
				)
			end
		end

		it 'populates trap.d directory' do
			expect(chef_run).to create_remote_directory('/etc/zabbix/trap.d')
			.with(
				files_backup: 0,
				files_owner: 'root',
				files_group: 'zabbix',
				files_mode: '0750',
				owner: 'root',
				group: 'root',
				source: 'traps'
			)
		end

		it 'renders mongo trap' do
			expect(chef_run).to create_template('/etc/zabbix/trap.d/mongo26.config')
			.with(
				owner: 'root',
				group: 'root',
				mode: '0700',
				source: 'mongo26.config.erb'
			)
			expect(chef_run).to render_file('/etc/zabbix/trap.d/mongo26.config')
			.with_content('mongoUser="unit_test_username"
mongoPass="unit_test_password"')
		end

		it 'renders links to trap scripts' do
			httplink = chef_run.link('/etc/zabbix/trap.d/live/httpd.sh')
			expect(chef_run).to create_link('/etc/zabbix/trap.d/live/httpd.sh')
			.with(to: '/etc/zabbix/trap.d/httpd.sh')
			expect(httplink).to link_to('/etc/zabbix/trap.d/httpd.sh')

			openldap2 = chef_run.link('/etc/zabbix/trap.d/live/openldap2.sh')
			expect(chef_run).to create_link('/etc/zabbix/trap.d/live/openldap2.sh')
			.with(to: '/etc/zabbix/trap.d/openldap2.sh')
			expect(openldap2).to link_to('/etc/zabbix/trap.d/openldap2.sh')

			sp = chef_run.link('/etc/zabbix/trap.d/live/sp.sh')
			expect(chef_run).to create_link('/etc/zabbix/trap.d/live/sp.sh')
			.with(to: '/etc/zabbix/trap.d/sp.sh')
			expect(sp).to link_to('/etc/zabbix/trap.d/sp.sh')

			idp = chef_run.link('/etc/zabbix/trap.d/live/idp.sh')
			expect(chef_run).to create_link('/etc/zabbix/trap.d/live/idp.sh')
			.with(to: '/etc/zabbix/trap.d/idp.sh')
			expect(idp).to link_to('/etc/zabbix/trap.d/idp.sh')

			tomcat = chef_run.link('/etc/zabbix/trap.d/live/tomcat.sh')
			expect(chef_run).to create_link('/etc/zabbix/trap.d/live/tomcat.sh')
			.with(to: '/etc/zabbix/trap.d/tomcat.sh')
			expect(tomcat).to link_to('/etc/zabbix/trap.d/tomcat.sh')

			mongo26 = chef_run.link('/etc/zabbix/trap.d/live/mongo26.sh')
			expect(chef_run).to create_link('/etc/zabbix/trap.d/live/mongo26.sh')
			.with(to: '/etc/zabbix/trap.d/mongo26.sh')
			expect(mongo26).to link_to('/etc/zabbix/trap.d/mongo26.sh')

			ssl_check_apache = chef_run.link('/etc/zabbix/trap.d/live/ssl_check_apache.sh')
			expect(chef_run).to create_link('/etc/zabbix/trap.d/live/ssl_check_apache.sh')
			.with(to: '/etc/zabbix/trap.d/ssl_check_apache.sh')
			expect(ssl_check_apache).to link_to('/etc/zabbix/trap.d/ssl_check_apache.sh')

			ssl_check_ldap = chef_run.link('/etc/zabbix/trap.d/live/ssl_check_ldap.sh')
			expect(chef_run).to create_link('/etc/zabbix/trap.d/live/ssl_check_ldap.sh')
			.with(to: '/etc/zabbix/trap.d/ssl_check_ldap.sh')
			expect(ssl_check_ldap).to link_to('/etc/zabbix/trap.d/ssl_check_ldap.sh')

			ssl_check_mongo = chef_run.link('/etc/zabbix/trap.d/live/ssl_check_mongo.sh')
			expect(chef_run).to create_link('/etc/zabbix/trap.d/live/ssl_check_mongo.sh')
			.with(to: '/etc/zabbix/trap.d/ssl_check_mongo.sh')
			expect(ssl_check_mongo).to link_to('/etc/zabbix/trap.d/ssl_check_mongo.sh')

		end

		it 'creates trapper crons' do
			expect(chef_run).to create_cron('zabbix_live')
			.with(
				minute: '*',
				hour: '*',
				user: 'root',
			)
			expect(chef_run).to_not create_cron('zabbix_daily')
			.with(
				minute: '*',
				hour: '0',
				user: 'root',
			)
		end

	end
end
