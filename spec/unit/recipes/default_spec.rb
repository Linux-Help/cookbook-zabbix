#
# Cookbook Name:: zabbix
# Spec:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

require 'spec_helper'

describe 'zabbix::default' do
  context 'When all attributes are default, on an unspecified platform' do
    let(:chef_run) { ChefSpec::SoloRunner.converge(described_recipe) }

    it 'calls zabbix agent recipe' do
        expect(chef_run).to include_recipe('zabbix::agent')
    end

  end
end
