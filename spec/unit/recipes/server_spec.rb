require 'spec_helper'

describe 'zabbix::server' do
    context 'When all database environments for zabbix are set on RHEL/CentOS' do
        let(:chef_run) do
            ChefSpec::SoloRunner.new do |node|
                # Create a new environment (you could also use a different :let block or :before block)
                env = Chef::Environment.new
                env.name 'unit_test'

                # Stub the node to return this environment
                allow(node).to receive(:chef_environment).and_return(env.name)

                # Stub any calls to Environment.load to return this environment
                allow(Chef::Environment).to receive(:load).and_return(env)

                # Stubbing out fqdn node attribute
                node.automatic['fqdn'] = 'unit.testing.stub'
            end.converge(described_recipe)
        end

        before(:each) do
          allow(Chef::EncryptedDataBagItem).to receive(:load).with('odhp_credentials', 'credentials').and_return(
          {
              'zabbix' => {
                  'users' => {
                      'unit_test_env' => {
                          'postgres' => {
                              'username' => 'postgres_username',
                              'password' => 'postgres_password'
                          }
                      }
                  }
              }
          })
          allow(Chef::EncryptedDataBagItem).to receive(:load).with('ssl', 'httpd').and_return(
          {
              'certificate' => 'unit_test_certificate_stub',
              'digicert1' => 'unit_test_digicert1_stub',
              'digicert2' => 'unit_test_digicert2_stub',
              'key' => 'unit_test_key_stub'
          })
        end

        let(:httpd_template_security) { chef_run.template('/etc/httpd/conf.d/security.conf') }
        let(:httpd_template_httpd) { chef_run.template('/etc/httpd/conf/httpd.conf') }
        let(:httpd_template_ssl) { chef_run.template('/etc/httpd/conf.d/ssl.conf') }
        let(:zabbix_httpd_template) { chef_run.template('/etc/httpd/conf.d/zabbix.conf') }
        let(:zabbix_server_template) { chef_run.template('/etc/zabbix/zabbix_server.conf') }
        let(:php_ini_template) { chef_run.template('/etc/php.ini') }

        it 'installs the httpd packages' do
          %w{httpd mod_ssl}.each do |pkg|
            expect(chef_run).to install_yum_package(pkg)
            .with(
                version: '2.2.15-39.el6',
                allow_downgrade: true
            )
          end
        end

        it 'installs the zabbix server packages' do
          %w{zabbix-server-pgsql zabbix-web-pgsql zabbix-get}.each do |pkg|
            expect(chef_run).to install_yum_package(pkg)
            .with(
                version: '3.0.5-1.el6',
                allow_downgrade: true
            )
          end
        end

        it 'installs the php packages' do
          %w{php56u php56u-bcmath php56u-gd php56u-mbstring php56u-pgsql php56u-xml php56u-xmlrpc php56u-cli php56u-opcache}.each do |pkg|
            expect(chef_run).to install_yum_package(pkg).with(
                version: '5.6.28-1.ius.centos6',
                allow_downgrade: true
            )
          end
        end

        it 'writes apache main configuration file' do
            expect(chef_run).to create_template('/etc/httpd/conf/httpd.conf')
            .with(
                owner: 'root',
                group: 'root',
                mode: '0644',
                source: 'httpd.conf.erb'
            )
            expect(httpd_template_security).to notify('service[httpd]').to(:restart).delayed
        end

        it 'creates zabbix.conf in httpd conf.d' do
          expect(chef_run).to create_template('/etc/httpd/conf.d/zabbix.conf')
          .with(
            source: 'zabbix_httpd.conf.erb',
            mode: '0644',
            owner: 'root',
            group: 'root'
          )
          expect(zabbix_httpd_template).to notify('service[httpd]').to(:restart).delayed
        end

        it 'writes apache security configuration' do
            expect(chef_run).to create_template('/etc/httpd/conf.d/security.conf')
            .with(
                owner: 'root',
                group: 'root',
                mode: '0644',
                source: 'security.conf.erb'
            )
            expect(httpd_template_security).to notify('service[httpd]').to(:restart).delayed
        end

        it 'writes apache ssl configuration' do
            expect(chef_run).to create_template('/etc/httpd/conf.d/ssl.conf') 
            .with(
                owner: 'root',
                group: 'root',
                mode: '0644',
                source: 'ssl.conf.erb'
            )
            expect(httpd_template_ssl).to notify('service[httpd]').to(:restart).delayed
        end

        it 'creates zabbix_server.conf' do
          expect(chef_run).to create_template('/etc/zabbix/zabbix_server.conf')
          .with(
              source: 'zabbix_server.conf.erb',
              mode: '0640',
              owner: 'root',
              group: 'root'
          )
          expect(zabbix_server_template).to notify('service[zabbix-server]').to(:restart).immediately
        end

        it 'creates zabbix.conf.php' do
          expect(chef_run).to create_template('/etc/zabbix/web/zabbix.conf.php')
          .with(
            source: 'zabbix_web.conf.erb',
            mode: '0640',
            owner: 'root',
            group: 'apache'
          )
        end

        it 'creates php.ini' do
          expect(chef_run).to create_template('/etc/php.ini')
          .with(
              source: 'php.ini.erb',
              mode: '0644',
              owner: 'root',
              group: 'root'
          )
          expect(php_ini_template).to notify('service[httpd]').to(:restart).immediately
        end

        it 'enables both start and enable actions for httpd' do
          expect(chef_run).to enable_service('httpd')
        end

        it 'enables both start and enable actions for zabbix-server' do
            expect(chef_run).to enable_service('zabbix-server')
        end

        it 'includes agent recipe' do
          expect(chef_run).to include_recipe('zabbix::agent')
        end
    end
end
