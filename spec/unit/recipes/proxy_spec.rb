require 'spec_helper'

describe 'zabbix::proxy' do
    context 'When all database environments for zabbix are set on RHEL/CentOS' do
        let(:chef_run) do
            ChefSpec::SoloRunner.new do |node|
                # Create a new environment (you could also use a different :let block or :before block)
                env = Chef::Environment.new
                env.name 'unit_test'

                # Stub the node to return this environment
                allow(node).to receive(:chef_environment).and_return(env.name)

                # Stub any calls to Environment.load to return this environment
                allow(Chef::Environment).to receive(:load).and_return(env)

                # Stubbing out fqdn node attribute
                node.automatic['fqdn'] = 'unit.testing.stub'
            end.converge(described_recipe)
        end

        before(:each) do
          allow(Chef::EncryptedDataBagItem).to receive(:load).with('odhp_credentials', 'credentials').and_return(
          {
              'zabbix' => {
                  'users' => {
                      'unit_test_env' => {
                          'postgres' => {
                              'username' => 'postgres_username',
                              'password' => 'postgres_password'
                          }
                      }
                  }
              }
          })
        end

        let(:zabbix_proxy_template) { chef_run.template('/etc/zabbix/zabbix_proxy.conf') }

        it 'installs the zabbix proxy packages' do
          %w{zabbix-proxy-sqlite3 zabbix-get}.each do |pkg|
            expect(chef_run).to install_yum_package(pkg)
            .with(
                version: '3.0.5-1.el6',
                allow_downgrade: true
            )
          end
        end

        it 'creates /var/lib/zabbix directory' do
          expect(chef_run).to create_directory('/var/lib/zabbix')
          .with(
              mode: '0750',
              owner: 'zabbix',
              group: 'zabbix'
          )
        end

        it 'creates zabbix_proxy.conf' do
          expect(chef_run).to create_template('/etc/zabbix/zabbix_proxy.conf')
          .with(
              source: 'zabbix_proxy.conf.erb',
              mode: '0640',
              owner: 'root',
              group: 'root'
          )
          expect(zabbix_proxy_template).to notify('service[zabbix-proxy]').to(:restart).immediately
        end

        it 'enables both start and enable actions for zabbix-proxy' do
            expect(chef_run).to enable_service('zabbix-proxy')
        end

        it 'includes agent recipe' do
          expect(chef_run).to include_recipe('zabbix::agent')
        end
    end
end
