Zabbix Cookbook
============

This cookbook installs, manages, and handles Zabbix for monitoring systems
health and system metrics. It also handles trap script auto-discovery based
on recipe inclusion and tags, as well as auto-registration metadata.

The way auto-discovery works is, for example, if the openldap recipe is
included in the run_list, the zabbix::trap_scripts will set up the variables
used to add certain trap scripts to the monitored system, for use in:`/etc/zabbix/trap.d/{live,daily}/`

Optionally, in case a recipe is incomplete or not in use, you can manually
enable a trap agent by use of tags on a per-node basis as well, using the
node tags as such:

```
{
  "name": "some.node.name",
  "chef_environment": "_default",
  "normal": {
    "tags": [
      "httpd",
      "openldap"
    ]
  },
  "run_list": [
    "recipe[zabbix]"
  ]
}
```

This allows for fully automated, and manual designation of agents required
for monitoring on every system.

Available tags for manual override are as follows:
```
httpd:  	Apache HTTPD
openldap:	OpenLDAP
mongodb:	MongoDB Database Server
tomcat:		Tomcat 6 Server
```
