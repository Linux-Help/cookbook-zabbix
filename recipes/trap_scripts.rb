#
# Cookbook Name:: zabbix
# Recipe:: trap_scripts
#
# Copyright 2017, Linux-Help.org
# Authors:
# 	Eric Renfro <psi-jack@linux-help.org>
#

node.override['zabbix']['trap_scripts']['live']['add'] = []
node.override['zabbix']['trap_scripts']['live']['del'] = []
node.override['zabbix']['trap_scripts']['daily']['add'] = []
node.override['zabbix']['trap_scripts']['daily']['del'] = []
node.override['zabbix']['agent_meta'] = []

# Utilize node tags to determine which agents to activate:

###########################################################
# Apache Web Server
#
if node['recipes'].include?('httpd') or node.tags.include?('httpd')
    node.override['zabbix']['trap_scripts']['live']['add'] += ['httpd.sh', 'ssl_check_apache.sh']
    node.override['zabbix']['agent_meta'] += ['HTTPD']
elsif
    node.override['zabbix']['trap_scripts']['live']['del'] += ['httpd.sh', 'ssl_check_apache.sh']
end

###########################################################
# OpenLDAP
#
if node['recipes'].include?('ldap') or node.tags.include?('openldap')
    node.override['zabbix']['trap_scripts']['live']['add'] += ['openldap2.sh', 'ssl_check_ldap.sh']
    node.override['zabbix']['agent_meta'] += ['OpenLDAP']
else
    node.override['zabbix']['trap_scripts']['live']['del'] += ['openldap2.sh', 'ssl_check_ldap.sh']
end

###########################################################
# MongoDB
#
if node['recipes'].include?('mongodb') or node.tags.include?('mongodb')
    node.override['zabbix']['trap_scripts']['live']['add'] += ['mongo26.sh', 'ssl_check_mongo.sh']
    node.override['zabbix']['agent_meta'] += ['MongoDB']
else
    node.override['zabbix']['trap_scripts']['live']['del'] += ['mongo26.sh', 'ssl_check_mongo.sh']
end

###########################################################
# Shibboleth Service Provider
#
if node['recipes'].include?('shibboleth') or node.tags.include?('shibboleth')
    node.override['zabbix']['trap_scripts']['live']['add'] += ['sp.sh', 'idp.sh', 'tomcat.sh']
    if not node.override['zabbix']['trap_scripts']['live']['add'].include?('httpd.sh')
        node.override['zabbix']['trap_scripts']['live']['add'] += ['httpd.sh']
    end
    if node.override['zabbix']['trap_scripts']['live']['del'].include?('httpd.sh')
        node.override['zabbix']['trap_scripts']['live']['del'].delete('httpd.sh')
    end
    if node['zabbix']['agent_meta'].include?('HTTPD')
        node.override['zabbix']['agent_meta'].delete('HTTPD')
    end
    node.override['zabbix']['agent_meta'] += ['ShibD']
else
    node.override['zabbix']['trap_scripts']['live']['del'] += ['sp.sh', 'idp.sh']
    if not node['recipes'].include?('tomcat')
        node.override['zabbix']['trap_scripts']['live']['del'] += ['tomcat.sh']
    end
end

###########################################################
# Tomcat
#
if (node['recipes'].include?('tomcat') or node.tags.include?('tomcat')) and
    not node.override['zabbix']['trap_scripts']['live']['add'].include?('tomcat.sh')
    node.override['zabbix']['trap_scripts']['live']['add'] += ['tomcat.sh']
    node.override['zabbix']['agent_meta'] += ['Tomcat6']
elsif not node['zabbix']['trap_scripts']['live']['del'].include?('tomcat.sh') and
    not node['zabbix']['trap_scripts']['live']['add'].include?('tomcat.sh') and
    not node.tags.include?('tomcat')
    node.override['zabbix']['trap_scripts']['live']['del'] += ['tomcat.sh']
end
