#
# Cookbook Name:: zabbix
# Recipe:: server_php
#
# Copyright 2017, Linux-Help.org
# Authors:
# 	Eric Renfro <psi-jack@linux-help.org>
#

#######################
# Install and Setup PHP
case node['platform_family']
when 'rhel'
  # Remove distro-provided versions if installed
  %w(php php-cli php-pear php-devel php-common).each do |pkg|
    package pkg do
      action :remove
    end
  end

  # Setup PHP to use yum-ius packages
  node.default['php']['packages'] = %w(php56u php56u-bcmath php56u-gd php56u-mbstring php56u-xml php56u-xmlrpc php56u-cli php56u-opcache)
  node.default['php']['fpm_package'] = 'php56u-fpm'
  node.default['php']['fpm_user'] = 'php-fpm'
  node.default['php']['fpm_group'] = 'php-fpm'
  node.default['php']['gd']['package'] = 'php56u-gd'
  node.default['php']['apcu']['package'] = 'php56u-pecl-apcu'
  node.default['php']['ldap']['package'] = 'php56u-ldap'

  if node['zabbix']['php']['use_fpm'] != true
    if node['zabbix']['webserver']['backend'] == 'apache'
      node.default['php']['packages'] += ['php56u']
    end
  end
  case node['zabbix']['database']['backend']
  when 'postgresql'
    node.default['php']['packages'] += ['php56u-pgsql']
    node.default['php']['postgresql']['package'] = 'php56u-pgsql'
  when 'mysql', 'mariadb', 'percona'
    node.default['php']['packages'] += ['php56u-mysqlnd']
    node.default['php']['mysql']['package'] = 'php56u-mysqlnd'
  end
end

# Set PHP timezone
node.default['php']['directives'] = {
  'date.timezone' => node['zabbix']['php']['timezone']
}
include_recipe 'php'

#############################
# Install PHP-FPM Zabbix pool
php_fpm_pool 'zabbix' do
  listen            '127.0.0.1:9001'
  user              'php-fpm'
  group             'php-fpm'
  #chdir             '/usr/share/zabbix'
  max_children      50
  start_servers     5
  min_spare_servers 5
  max_spare_servers 35
  additional_config({
    'pm.process_idle_timeout'         => '10s',
    'pm.max_requests'                 => '500',
    'ping.path'                       => '/ping',
    'ping.response'                   => 'pong',
    'php_flag[display_errors]'        => 'off',
    'php_admin_value[error_log]'      => '/var/log/php-fpm/www-error.log',
    'php_admin_flag[log_errors]'      => 'on',
    'php_admin_value[memory_limit]'   => '128M',
    'php_value[session.save_handler]' => 'files',
    'php_value[session.save_path]'    => '/var/lib/php-fpm/session',
    'php_value[soap.wsdl_cache_dir]'  => '/var/lib/php-fpm/wsdlcache',
    'php_value[max_execution_time]'   => '300',
    'php_value[post_max_size]'        => '16M',
    'php_value[upload_max_filesize]'  => '2M',
    'php_value[max_input_time]'       => '300',
    'php_value[always_populate_raw_post_data]' => '-1'
  })
  action :install
  only_if { node['zabbix']['php']['use_fpm'] || node['zabbix']['webserver']['backend'] == 'nginx' }
end
