#
# Cookbook Name:: zabbix
# Recipe:: database_postgresql
#
# Copyright 2017, Linux-Help.org
# Authors:
# 	Eric Renfro <psi-jack@linux-help.org>
#

include_recipe 'chef-vault'

if node['zabbix']['database']['backend'] == 'postgresql'
  if node['zabbix']['database']['repo']['pgdg']
    node.default['postgresql']['enable_pgdg_apt'] = true
    node.default['postgresql']['enable_pgdg_yum'] = true
    node.default['postgresql']['version'] = "9.6"
  end
  node.default['postgresql']['pg_hba'] = [
    { type: 'local', db: 'all', user: 'postgres', addr: nil, method: 'ident' },
    { type: 'local', db: 'all', user: 'all', addr: nil, method: 'md5' },
    { type: 'host', db: 'all', user: 'all', addr: '127.0.0.1/32', method: 'md5' },
    { type: 'host', db: 'all', user: 'all', addr: '::1/128', method: 'md5' },
  ]
  include_recipe 'postgresql::server'
else
  Chef::Application.fatal!('database_postgresql, but backend is set differently.', 111)
end

directory '/tmp/database' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
  not_if "sudo -u postgres psql -lqtA | grep -q '^zabbix'"
end

cookbook_file '/tmp/database/schema.sql' do
  sensitive true
  source %W{
    host-#{node['fqdn']}/schema.sql
    #{node['zabbix']['database']['backend']}/#{node['zabbix']['version']}/schema.sql
  }
  backup false
  action :nothing
  subscribes :create, 'directory[/tmp/database]', :immediately
end

cookbook_file '/tmp/database/images.sql' do
  sensitive true
  source %W{
    host-#{node['fqdn']}/images.sql
    #{node['zabbix']['database']['backend']}/#{node['zabbix']['version']}/images.sql
  }
  backup false
  action :nothing
  subscribes :create, 'directory[/tmp/database]', :immediately
end

cookbook_file '/tmp/database/data.sql' do
  sensitive true
  source %W{
    host-#{node['fqdn']}/data.sql
    #{node['zabbix']['database']['backend']}/#{node['zabbix']['version']}/data.sql
  }
  backup false
  action :nothing
  subscribes :create, 'directory[/tmp/database]', :immediately
end

bash 'create_zabbix_db_user' do
  user 'postgres'
  #sensitive true
  credentials = chef_vault_item("secrets", "zabbix")
  code <<-EOH
    psql -c "CREATE USER \"#{credentials['username']}\" WITH PASSWORD '#{credentials['password']}';"
    psql -c "CREATE DATABASE \"#{credentials['database']}\" WITH OWNER \"#{credentials['username']}\" ENCODING 'UTF-8';"
  EOH
  action :nothing
  subscribes :run, 'directory[/tmp/database]', :immediately
end

bash 'initialize_zabbix_db' do
  sensitive true
  credentials = chef_vault_item("secrets", "zabbix")
  environment({
    "PGUSER" => credentials['username'],
    "PGPASSWORD" => credentials['password']
  })
  code <<-EOH
    psql -d zabbix -f /tmp/database/schema.sql
    psql -d zabbix -f /tmp/database/images.sql
    psql -d zabbix -f /tmp/database/data.sql
  EOH
  action :nothing
  subscribes :run, 'directory[/tmp/database]', :immediately
end

directory "cleanup" do
    path "/tmp/database"
    recursive true
    action :nothing
    subscribes :delete, 'directory[/tmp/database]', :immediately
end
