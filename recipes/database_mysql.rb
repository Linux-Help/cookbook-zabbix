#
# Cookbook Name:: zabbix
# Recipe:: database_mysql
#
# Copyright 2017, Linux-Help.org
# Authors:
# 	Eric Renfro <psi-jack@linux-help.org>
#

include_recipe 'chef-vault'

if not node['zabbix']['database']['backend'] == 'mysql'
  Chef::Application.fatal!('database_mysql, but backend is set differently.', 111)
end

mysql_service 'zabbix' do
  port '3306'
  initial_root_password 'password'
  action [:create, :start]
end

directory '/tmp/database' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
  not_if "true"
end

cookbook_file '/tmp/database/schema.sql' do
  source %W{
    host-#{node['fqdn']}/schema.sql
    #{node['zabbix']['database']['backend']}/#{node['zabbix']['version']}/schema.sql
  }
  backup false
  action :nothing
  subscribes :create, 'directory[/tmp/database]', :immediately
end

cookbook_file '/tmp/database/images.sql' do
  source %W{
    host-#{node['fqdn']}/images.sql
    #{node['zabbix']['database']['backend']}/#{node['zabbix']['version']}/images.sql
  }
  backup false
  action :nothing
  subscribes :create, 'directory[/tmp/database]', :immediately
end

cookbook_file '/tmp/database/data.sql' do
  source %W{
    host-#{node['fqdn']}/data.sql
    #{node['zabbix']['database']['backend']}/#{node['zabbix']['version']}/data.sql
  }
  backup false
  action :nothing
  subscribes :create, 'directory[/tmp/database]', :immediately
end

bash 'create_zabbix_db_user' do
  user 'postgres'
  sensitive true
  credentials = chef_vault_item("secrets", "zabbix")
  code <<-EOH
    psql -c "CREATE DATABASE zabbix WITH ENCODING='UTF-8';"
    psql -c "CREATE USER zabbix WITH PASSWORD '#{credentials['postgres']}';"
    psql -c "GRANT ALL PRIVILEGES ON DATABASE zabbix TO zabbix;"
  EOH
  action :nothing
  subscribes :run, 'directory[/tmp/database]', :immediately
end

bash 'initialize_zabbix_db' do
  sensitive true
  user 'postgres'
  code <<-EOH
    psql -d zabbix -f /tmp/database/schema.sql
    psql -d zabbix -f /tmp/database/images.sql
    psql -d zabbix -f /tmp/database/data.sql
  EOH
  action :nothing
  subscribes :run, 'directory[/tmp/database]', :immediately
end

directory "cleanup" do
    path "/tmp/database"
    recursive true
    action :nothing
    subscribes :delete, 'directory[/tmp/database]', :immediately
end
