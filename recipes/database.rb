#
# Cookbook Name:: zabbix
# Recipe:: database
#
# Copyright 2017, Linux-Help.org
# Authors:
# 	Eric Renfro <psi-jack@linux-help.org>
#

case node['zabbix']['database']['backend']
when 'postgresql'
  include_recipe 'zabbix::database_postgresql'
else
  Chef::Application.fatal!('Unknown database backend to use.', 101)
end
