#
# Cookbook Name:: zabbix
# Recipe:: default
#
# Copyright 2014, Mobiquity, Inc.
# Authors:
# 	Eric Renfro <erenfro@mobiquityinc.com>
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'zabbix::agent'

