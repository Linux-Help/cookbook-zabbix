#
# Cookbook Name:: zabbix
# Recipe:: server_nginx
#
# Copyright 2017, Linux-Help.org
# Authors:
# 	Eric Renfro <psi-jack@linux-help.org>
#

#########################
# Setup and Install nginx
node.default['nginx']['default_site_enabled'] = false
include_recipe 'chef_nginx'

nginx_site "zabbix" do
  template 'nginx/zabbix.conf.erb'
  variables({
    :params => {
      :server_port  => node['zabbix']['webserver']['port'],
      :ssl_port     => node['zabbix']['ssl']['port'],
      :server_name  => [ "#{node['fqdn']}, zabbix.#{node['domain']}" ],
      :docroot      => '/usr/share/zabbix',
      :cacert       => node['zabbix']['ssl']['cacert'],
      :server_cert  => node['zabbix']['ssl']['server_cert'],
      :server_key   => node['zabbix']['ssl']['server_key'],
      :ssl_proto    => node['zabbix']['ssl']['ssl_protocols'],
      :ssl_ciphers  => node['zabbix']['ssl']['ssl_ciphers']
    }
  })
end

file "/etc/nginx/conf.d/default.conf" do
  action :delete
end
