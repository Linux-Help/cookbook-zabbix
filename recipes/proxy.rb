#
# Cookbook Name:: zabbix
# Recipe:: proxy
#
# Copyright 2017, Linux-Help.org
# Authors:
# 	Eric Renfro <psi-jack@linux-help.org>
#

include_recipe 'chef-vault'

%w{zabbix-proxy-sqlite3 zabbix-get}.each do |pkg|
    yum_package pkg do
        version '3.0.5-1.el6'
        allow_downgrade true
        action :install
    end
end

directory "/var/lib/zabbix" do
  owner 'zabbix'
  group 'zabbix'
  mode '0750'
  action :create
end

template "/etc/zabbix/zabbix_proxy.conf" do
  credentials = chef_vault_item("secrets", "zabbix")
  variables({
    :DBUsername => credentials['username'],
    :DBPassword => credentials['password'],
    :DBDatabase => credentials['database']
  })
  source %W{
    zabbix/#{node['zabbix']['version']}/#{node['platform']}-#{node['platform_version'].to_i}/zabbix_proxy.conf.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform']}/zabbix_proxy.conf.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform_family']}-#{node['platform_version'].to_i}/zabbix_proxy.conf.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform_family']}/zabbix_proxy.conf.erb
    zabbix/#{node['zabbix']['version']}/zabbix_proxy.conf.erb
    zabbix/zabbix_proxy.conf.erb
    default/zabbix_proxy.conf.erb
    zabbix_proxy.conf.erb
  }
  sensitive true
  mode "0640"
  owner "root"
  group "root"
  notifies :restart, "service[zabbix-proxy]", :immediately
end

service 'zabbix-proxy' do
  supports :restart => true, :start => true
  action :enable
end

include_recipe 'zabbix::agent'
