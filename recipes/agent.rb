#
# Cookbook Name:: zabbix
# Recipe:: agent
#
# Copyright 2017, Linux-Help.org
# Authors:
# 	Eric Renfro <psi-jack@linux-help.org>
#

include_recipe 'zabbix::trap_scripts'

# Install zabbix package and dependencies
%w{zabbix-agent zabbix-sender cronie crontabs}.each do |pkg|
  yum_package pkg
end

# Install custom zabbix_agentd.conf
template "/etc/zabbix/zabbix_agentd.conf" do
  source %W{
    zabbix/#{node['zabbix']['version']}/#{node['platform']}-#{node['platform_version'].to_i}/zabbix_agentd.conf.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform']}/zabbix_agentd.conf.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform_family']}-#{node['platform_version'].to_i}/zabbix_agentd.conf.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform_family']}/zabbix_agentd.conf.erb
    zabbix/#{node['zabbix']['version']}/zabbix_agentd.conf.erb
    zabbix/zabbix_agentd.conf.erb
    default/zabbix_agentd.conf.erb
    zabbix_agentd.conf.erb
  }
  mode "0644"
  owner "root"
  group "root"
  notifies :restart, "service[zabbix-agent]", :delayed
end

# Define the zabbix service, set to start on boot
service "zabbix-agent" do
  supports :restart => true, :status => true, :reload => false
  action [:enable, :start]
end

# Install custom trap scripts.
%w[ /etc/zabbix/trap.d /etc/zabbix/trap.d/live /etc/zabbix/trap.d/daily ].each do |path|
  directory path do
    owner 'root'
    group 'root'
    mode '0755'
  end
end

remote_directory "/etc/zabbix/trap.d" do
  files_backup 0
  files_owner 'root'
  files_group 'zabbix'
  files_mode '0750'
  owner 'root'
  group 'root'
  source 'traps'
  action :create
end

if node['recipes'].include?('mongodb') or node['tags'].include?('mongodb')
  template "/etc/zabbix/trap.d/mongo26.config" do
    owner "root"
    group "root"
    mode "0700"
    source "mongo26.config.erb"
    sensitive true
    credentials = Chef::EncryptedDataBagItem.load("odhp_credentials", "credentials")
    variables({
        :users => credentials['mongo']['users'][node.chef_environment]
    })
  end
else
  file "/etc/zabbix/trap.d/mongo26.config" do
    action :delete
  end
end


# Link live agents to node
node['zabbix']['trap_scripts']['live']['add'].each do |script|
  link "/etc/zabbix/trap.d/live/#{script}" do
    to "/etc/zabbix/trap.d/#{script}"
  end
end

# Link daily agents to node
node['zabbix']['trap_scripts']['daily']['add'].each do |script|
  link "/etc/zabbix/trap.d/daily/#{script}" do
    to "/etc/zabbix/trap.d/#{script}"
  end
end

# Delete live agents to node
node['zabbix']['trap_scripts']['live']['del'].each do |script|
  link "/etc/zabbix/trap.d/live/#{script}" do
    to "/etc/zabbix/trap.d/#{script}"
    action :delete
  end
end

# Delete daily agents to node
node['zabbix']['trap_scripts']['daily']['del'].each do |script|
  link "/etc/zabbix/trap.d/daily/#{script}" do
    to "/etc/zabbix/trap.d/#{script}"
    action :delete
  end
end


# Setup trapper crons according to agents setup as appropriate:
cron "zabbix_live" do
  minute '*'
  hour '*'
  user "root"
  command %Q{/etc/zabbix/trap.d/runtrap live >/dev/null 2>&1}
  only_if { node['zabbix']['trap_scripts']['live']['add'].any? }
end

cron "zabbix_daily" do
  minute '*'
  hour '0'
  user "root"
  command %Q{/etc/zabbix/trap.d/runtrap daily >/dev/null 2>&1}
  only_if { node['zabbix']['trap_scripts']['daily']['add'].any? }
end
