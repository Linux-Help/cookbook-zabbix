#
# Cookbook Name:: zabbix
# Recipe:: server
#
# Copyright 2017, Linux-Help.org
# Authors:
# 	Eric Renfro <psi-jack@linux-help.org>
#

include_recipe 'chef-vault'
include_recipe "#{cookbook_name}::database"

#################################
# Include Additional Repositories
case node['platform_family']
when 'rhel'
  include_recipe 'yum-ius'
  include_recipe 'yum-zabbix'
end

#############################
# Setup and Install Webserver
case node['zabbix']['webserver']['backend']
when 'apache'
  include_recipe "#{cookbook_name}::server_apache"
when 'nginx'
  include_recipe "#{cookbook_name}::server_nginx"
else
  Chef::Application.fatal!('Webserver backend can either be apache or nginx', 121)
end

#######################
# Setup and Install PHP
include_recipe "#{cookbook_name}::server_php"

#######################
# Install Zabbix Server
case node['zabbix']['database']['backend']
when 'postgresql'
  %w(zabbix-server-pgsql zabbix-web-pgsql zabbix-get).each do |pkg|
    package pkg
  end
when 'mysql', 'mariadb', 'percona'
  %w(zabbix-server-mysql zabbix-web-mysql zabbix-get).each do |pkg|
    package pkg
  end
end

#########################
# Configure Zabbix Server
template "/etc/zabbix/zabbix_server.conf" do
  credentials = chef_vault_item("secrets", "zabbix")
  variables({
    :DBUsername => credentials['username'],
    :DBPassword => credentials['password'],
    :DBDatabase => credentials['database']
  })
  source %W{
    zabbix/#{node['zabbix']['version']}/#{node['platform']}-#{node['platform_version'].to_i}/zabbix_server.conf.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform']}/zabbix_server.conf.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform_family']}-#{node['platform_version'].to_i}/zabbix_server.conf.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform_family']}/zabbix_server.conf.erb
    zabbix/#{node['zabbix']['version']}/zabbix_server.conf.erb
    zabbix/zabbix_server.conf.erb
    default/zabbix_server.conf.erb
    zabbix_server.conf.erb
  }
  sensitive true
  mode  "0640"
  owner "root"
  group "root"
  notifies :restart, "service[zabbix-server]", :immediately
end

directory "/etc/zabbix/web" do
  mode  "0750"
  owner "root"
  group case node['zabbix']['php']['use_fpm']
  when true
    node['php']['fpm_group']
  else
    case node['zabbix']['webserver']['backend']
    when 'apache'
      node['apache']['group']
    when 'nginx'
      node['php']['fpm_group']
    end
  end
end

template "/etc/zabbix/web/zabbix.conf.php" do
  credentials = chef_vault_item("secrets", "zabbix")
  variables({
    :DBUsername => credentials['username'],
    :DBPassword => credentials['password'],
    :DBDatabase => credentials['database']
  })
  source %W{
    zabbix/#{node['zabbix']['version']}/#{node['platform']}-#{node['platform_version'].to_i}/zabbix.conf.php.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform']}/zabbix.conf.php.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform_family']}-#{node['platform_version'].to_i}/zabbix.conf.php.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform_family']}/zabbix.conf.php.erb
    zabbix/#{node['zabbix']['version']}/zabbix.conf.php.erb
    zabbix/zabbix.conf.php.erb
    default/zabbix.conf.php.erb
    zabbix.conf.php.erb
  }
  sensitive true
  mode  "0640"
  owner "root"
  group case node['zabbix']['php']['use_fpm']
  when true
    node['php']['fpm_group']
  else
    case node['zabbix']['webserver']['backend']
    when 'apache'
      node['apache']['group']
    when 'nginx'
      node['php']['fpm_group']
    end
  end
end

template "/etc/zabbix/web/maintenance.inc.php" do
  source %W{
    zabbix/#{node['zabbix']['version']}/#{node['platform']}-#{node['platform_version'].to_i}/maintenance.inc.php.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform']}/maintenance.inc.php.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform_family']}-#{node['platform_version'].to_i}/maintenance.inc.php.erb
    zabbix/#{node['zabbix']['version']}/#{node['platform_family']}/maintenance.inc.php.erb
    zabbix/#{node['zabbix']['version']}/maintenance.inc.php.erb
    zabbix/maintenance.inc.php.erb
    default/maintenance.inc.php.erb
    maintenance.inc.php.erb
  }
  sensitive true
  mode  "0640"
  owner "root"
  group case node['zabbix']['php']['use_fpm']
  when true
    node['php']['fpm_group']
  else
    case node['zabbix']['webserver']['backend']
    when 'apache'
      node['apache']['group']
    when 'nginx'
      node['php']['fpm_group']
    end
  end
end

#################
# Manage Services
service 'zabbix-server' do
  supports :restart => true, :start => true
  action [:enable, :start]
  subscribes :restart, "template[/etc/zabbix/zabbix_server.conf]", :delayed
end

service 'php-fpm' do
  action [:enable, :start]
  subscribes :reload, "php_fpm_pool[default]", :delayed
  only_if { node['zabbix']['php']['use_fpm'] }
end

######################
# Include Zabbix Agent
include_recipe 'zabbix::agent'
