#
# Cookbook Name:: zabbix
# Recipe:: server_apache
#
# Copyright 2017, Linux-Help.org
# Authors:
# 	Eric Renfro <psi-jack@linux-help.org>
#

include_recipe 'apache2'

##########################
# Setup and Install Apache
%w(proxy proxy_fcgi).each do |mod|
  apache_module mod do
    only_if { node['zabbix']['php']['use_fpm'] }
  end
end

web_app "zabbix" do
  tname = node['zabbix']['php']['use_fpm'] ? "zabbix-fpm" : "zabbix-mod"
  server_name       node['fqdn']
  server_aliases    ["zabbix.#{node['domain']}"]
  template %W{
    apache/#{node['platform']}/#{node['platform_version'].to_i}/#{tname}.conf.erb
    apache/#{node['platform']}/#{tname}.conf.erb
    apache/#{node['platform_family']}/#{node['platform_version'].to_i}/#{tname}.conf.erb
    apache/#{node['platform_family']}/#{tname}.conf.erb
    apache/default/#{tname}.conf.erb
    apache/#{tname}.conf.erb
  }
  docroot           "/usr/share/zabbix"
  directory_index   ["index.php"]
  directory_options [ "FollowSymLinks" ]
  allow_override    [ "None" ]
end
