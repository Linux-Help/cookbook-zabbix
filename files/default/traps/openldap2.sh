#!/bin/bash

/etc/init.d/slapd status >/dev/null 2>&1
status=$?

if [[ $status -ne 0 ]]
then
	echo "- ldap.status 0"
else
	echo "- ldap.status 1"
fi

