#!/bin/bash

status=$(curl -ks https://localhost/Shibboleth.sso/Status)
statusErr=$?

if [[ $statusErr -eq 0 ]]
then
	if grep -q '<Status><OK/></Status>' <<< "$status"
	then
		echo "- shibboleth.sp.status 1"
	else
		echo "- shibboleth.sp.status 0"
	fi
else
	echo "- shibboleth.sp.status 0"
fi

