#!/bin/bash

procs=$(/etc/init.d/httpd status >/dev/null 2>&1)
ret=$?

if [[ $ret -eq 0 ]]
then
	echo "- apache.status 1"
else
	echo "- apache.status 0"
fi

