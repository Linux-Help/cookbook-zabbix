#!/bin/bash

search=$(ldapsearch -x -H ldapi:/// -b '' -s base '(objectClass=*)' namingConexts | grep -wc "Success")

if [[ $search -gt 0 ]]
then
	echo "- ldap.search 1"
else
	echo "- ldap.search 0"
fi

#port=$(zabbix_agent -t net.tcp.port[,636] | cut -d'|' -f2 | cut -d']' -f1)
#echo "- ldap.port $port"

port=$(ss -lnp 2>&1 | grep ':636' | wc -l)
if [[ $port -gt 0 ]]
then
	echo "- ldap.port 1"
else
	echo "- ldap.port 0"
fi

