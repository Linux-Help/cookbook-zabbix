#!/bin/bash

curl -ks https://localhost/idp/status | grep "idp_version" >/dev/null 2>&1
ret=$?

if [[ "$ret" -ne 0 ]]
then
	echo "- shibboleth.idp.status 0"
else
	echo "- shibboleth.idp.status 1"
fi

