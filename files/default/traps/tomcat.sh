#!/bin/bash

/etc/init.d/tomcat6 status >/dev/null 2>&1
ret=$?

if [[ $ret -eq 0 ]]
then
	echo "- tomcat.status 1"
else
	echo "- tomcat.status 0"
fi

