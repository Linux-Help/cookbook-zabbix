#! /bin/bash

configPath="$(dirname $(readlink -f $0))"
if [[ -r "${configPath}/mongo26.config" ]]
then
	source "${configPath}/mongo26.config"
else
	exit 0
fi

# A single mandatory parameter: the Zabbix Server host
# Send just the first 7 values
fields=(insert query update delete getmore command flushes mapped vsize res faults locked_db idx_miss_% "qr|qw" "ar|aw" netIn netOut conn time)
#sendIDX=(0 1 2 3 4 5 7 12 13 14 15 16 17 19 20)
sendIDX=(0 1 2 3 4 5 6 10 12 13 14 17)
#IFS=$' \t\n|'

set -o pipefail
declare -A mongostat

eval $(mongostat --username ${mongoUser} --password ${mongoPass} --ssl -n 1 6 2>&1 | (
	OLDIFS=$IFS
	IFS=$' \t\n'
	read test1;
	read test2;
	#echo "test1=$test1"
	#echo "test2=$test2"
	if [[ "$test2" != "insert  query update delete getmore command flushes mapped  vsize    res faults      locked db idx miss %     qr|qw   ar|aw  netIn netOut  conn       time" ]]
	then
		exit 2
	fi
	read -a values;
	curField=0
	for i in ${sendIDX[@]} 
	do
		if [[ "${fields[i]}" = "command" ]]
		then
			IFS='|' read -a subvalues <<< "${values[i]}"
				echo "mongostat[command]=${subvalues[0]}"
		elif [[ "${fields[i]}" = "qr|qw" ]]
		then
			IFS='|' read -a subvalues <<< "${values[i]}"
				echo "mongostat[qr]=${subvalues[0]}"
				echo "mongostat[qw]=${subvalues[1]}"
		elif [[ "${fields[i]}" = "ar|aw" ]]
		then
			IFS='|' read -a subvalues <<< "${values[i]}"
				echo "mongostat[ar]=${subvalues[0]}"
				echo "mongostat[aw]=${subvalues[1]}"
		else
			if [[ $curField -lt 4 ]]
			then
				echo "mongostat[${fields[i]}]=${values[i]//\*}"
			else
				echo "mongostat[${fields[i]}]=${values[i]}"
			fi
		fi
		let curField++
	done
	IFS=$OLDIFS
))
ret=$?
if [[ $ret -ne 0 ]]
then
	echo "- mongostat[status] 0"
else
	for v in "${!mongostat[@]}"
	do
		echo "- mongostat[$v] ${mongostat[$v]}"
	done
	echo "- mongostat[status] 1"
fi

