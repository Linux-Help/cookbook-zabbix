require 'spec_helper'

describe 'zabbix::database' do
    describe "Installed Packages:" do
        describe package('postgresql94-server') do
            it { should be_installed }
        end
    end

    describe "Configuration Files:" do
        describe file('/var/lib/pgsql/9.4/data/pg_hba.conf') do
            it { should be_file }
            it { should be_mode 600 }
            it { should be_owned_by 'postgres' }
            it { should be_grouped_into 'postgres' }
        end
    end

    describe "Services:" do
        describe service('postgresql-9.4') do
            it { should be_enabled }
            it { should be_running }
        end

        describe process('postmaster') do
            its(:user) { should eq 'postgres' }
        end

        describe port(5432) do
            it { should be_listening.on('127.0.0.1').with('tcp') }
        end
    end
end
