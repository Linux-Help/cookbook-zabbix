require 'spec_helper'

describe 'zabbix::server' do
    describe "Installed Packages:" do
        %w{httpd mod_ssl}.each do |pkg|
            describe package(pkg) do
                it { should be_installed.with_version('2.2.15-39.el6') }
            end
        end

        %w{php56u php56u-bcmath php56u-gd php56u-mbstring php56u-pgsql php56u-xml php56u-xmlrpc php56u-cli php56u-opcache}.each do |pkg|
            describe package(pkg) do
                it { should be_installed.with_version('5.6.28-1.ius.centos6') }
            end
        end

        %w{zabbix-server-pgsql zabbix-web-pgsql zabbix-get zabbix-agent}.each do |pkg|
            describe package(pkg) do
                it { should be_installed.with_version('3.0.5-1.el6') }
            end
        end
    end

    describe "Configuration Files:" do
        describe file('/etc/httpd/conf/httpd.conf') do
            it { should be_file }
            it { should be_mode 644 }
            it { should be_owned_by 'root' }
            it { should be_grouped_into 'root' }
        end

        describe file('/etc/httpd/conf.d/security.conf') do
            it { should be_file }
            it { should be_mode 644 }
            it { should be_owned_by 'root' }
            it { should be_grouped_into 'root' }
        end

        describe file('/etc/httpd/conf.d/ssl.conf') do
            it { should be_file }
            it { should be_mode 644 }
            it { should be_owned_by 'root' }
            it { should be_grouped_into 'root' }
        end

        describe file('/etc/zabbix/zabbix_server.conf') do
            it { should be_file }
            it { should be_mode 640 }
            it { should be_owned_by 'root' }
            it { should be_grouped_into 'root' }
            its(:sha256sum) { should eq '1bd8289af12ada9489b67ac04e1f1dc0ed6736bbebab61153ce85dcbe72b8289' }
        end

        describe file('/etc/php.ini') do
            it { should be_file }
            it { should be_mode 644 }
            it { should be_owned_by 'root' }
            it { should be_grouped_into 'root' }
            its(:sha256sum) { should eq '9b34f15b5bb38627130ced110062f1d9f60aece21de65475ac7b660a9ddbfc71' }
        end
    end

    describe "Services:" do
        describe service('httpd') do
            it { should be_enabled }
            it { should be_running }
        end

        describe service('zabbix-server') do
            it { should be_enabled }
            it { should be_running }
        end

        describe process('zabbix_server') do
            its(:user) { should eq "zabbix" }
        end

        describe port(80) do
            it { should be_listening.with('tcp') }
        end

        #describe port(443) do
        #    it { should be_listening.with('tcp') }
        #end

        describe port(10051) do
            it { should be_listening.with('tcp') }
        end
    end
end
