require 'spec_helper'

describe 'zabbix::proxy' do
    describe "Installed Packages:" do
        %w{zabbix-proxy-sqlite3 zabbix-agent zabbix-get}.each do |pkg|
            describe package(pkg) do
                it { should be_installed }
            end
        end
    end

    describe "Configuration Files:" do
        describe file('/etc/zabbix/zabbix_proxy.conf') do
            it { should be_file }
            it { should be_mode 640 }
            it { should be_owned_by 'root' }
            it { should be_grouped_into 'root' }
            its(:sha256sum) { should eq 'fd1620dfc6f13eeb8ef4d7bdace3dc16bababdf0a46db787d579bdd9ae0c7577' }
        end

        describe file('/var/lib/zabbix') do
            it { should exist }
            it { should be_directory }
            it { should be_mode 750 }
            it { should be_owned_by 'zabbix' }
            it { should be_grouped_into 'zabbix' }
        end

        describe file('/var/lib/zabbix/zabbix_proxy.db') do
            it { should exist }
            it { should be_file }
            it { should be_mode 644 }
            it { should be_owned_by 'zabbix' }
            it { should be_grouped_into 'zabbix' }
        end
    end

    describe "Services:" do
        describe service('zabbix-proxy') do
            it { should be_enabled }
            it { should be_running }
        end

        describe process('zabbix_proxy') do
            its(:user) { should eq "zabbix" }
        end

        describe port(10051) do
            it { should be_listening.with('tcp') }
        end
    end
end

