require 'spec_helper'

describe 'zabbix::default' do

    # Serverspec examples can be found at
    # http://serverspec.org/resource_types.html

    describe "Installed Packages:" do
        %w{zabbix-agent zabbix-sender wget}.each do |pkg|
          describe package(pkg) do
              it { should be_installed }
          end
        end
    end

    describe "Configuration Files:" do
        describe file('/etc/zabbix/zabbix_agentd.conf') do
            it { should be_file }
            it { should be_mode 644 }
            it { should be_owned_by 'root' }
            it { should be_grouped_into 'root' }
            its(:sha256sum) { should eq '9d26e61c0f10f3667ee69046a6b253b1af9cb4c73f2d95c432a7359cdeb9c79d' }
        end
    end

    describe "Zabbix Scripts:" do
        describe port(10050) do
            it { should be_listening.with('tcp') }
        end

        %w[ /etc/zabbix/trap.d /etc/zabbix/trap.d/live /etc/zabbix/trap.d/daily ].each do |path|
            describe file(path) do
                it { should be_directory }
                it { should be_owned_by 'root' }
                it { should be_grouped_into 'root' }
                it { should be_mode 755 }
            end
        end

        describe file('/etc/zabbix/trap.d/mongo26.config') do
            it { should be_file }
            it { should be_mode 700 }
            it { should be_owned_by 'root' }
            it { should be_grouped_into 'root' }
            its(:sha256sum) { should eq '4feb7eb028cbac16ef4bcd9fc435cc100ed84f64142a410a79ca52f1a8c73526' }
        end

        describe file('/etc/zabbix/trap.d/live/httpd.sh') do
            it { should be_symlink }
            it { should be_linked_to '/etc/zabbix/trap.d/httpd.sh' }
            its(:sha256sum) { should eq 'f49e60bb9e85674eef968333e78c76e5c47ad9e850e3718297f8de060c556543' }
        end
        describe file('/etc/zabbix/trap.d/live/idp.sh') do
            it { should be_symlink }
            it { should be_linked_to '/etc/zabbix/trap.d/idp.sh' }
            its(:sha256sum) { should eq '091d86d3472307360556f3ce42656ee51a2530ad07e0038e2002d09a5d9545a1' }
        end
        describe file('/etc/zabbix/trap.d/live/mongo26.sh') do
            it { should be_symlink }
            it { should be_linked_to '/etc/zabbix/trap.d/mongo26.sh' }
            its(:sha256sum) { should eq '8a1fcebd0699395b9a1f43ba73c5f71eb1de5c9a181f0d8e450eebf337974d6f' }
        end
        describe file('/etc/zabbix/trap.d/live/openldap2.sh') do
            it { should be_symlink }
            it { should be_linked_to '/etc/zabbix/trap.d/openldap2.sh' }
            its(:sha256sum) { should eq '8aa432ea0f0db9153183210e832d3b2c744cfd2893dbc370c240b6edf2583871' }
        end
        describe file('/etc/zabbix/trap.d/live/sp.sh') do
            it { should be_symlink }
            it { should be_linked_to '/etc/zabbix/trap.d/sp.sh' }
            its(:sha256sum) { should eq '04b7fcc0d1e520b9029e5b5fb4a68b13e79160afb3782875f23fd0b33e0db1e3' }
        end
        describe file('/etc/zabbix/trap.d/live/tomcat.sh') do
            it { should be_symlink }
            it { should be_linked_to '/etc/zabbix/trap.d/tomcat.sh' }
            its(:sha256sum) { should eq '0a5d29eaab50af586c1546003458423afd0056ba43bb894e305ed5c40eabd57d' }
        end
        describe file('/etc/zabbix/trap.d/live/ssl_check_apache.sh') do
            it { should be_symlink }
            it { should be_linked_to '/etc/zabbix/trap.d/ssl_check_apache.sh' }
            its(:sha256sum) { should eq 'c416956452b40ed0de02f0b83279f13030c79c562b55f6fe373fcb24ab3f3d5b' }
        end
        describe file('/etc/zabbix/trap.d/live/ssl_check_ldap.sh') do
            it { should be_symlink }
            it { should be_linked_to '/etc/zabbix/trap.d/ssl_check_ldap.sh' }
            its(:sha256sum) { should eq 'ed2a8e7da1bc4f7d53c52e90ad3e7b7c1a418dc223ec99d4d1381988f2f02f9f' }
        end
        describe file('/etc/zabbix/trap.d/live/ssl_check_mongo.sh') do
            it { should be_symlink }
            it { should be_linked_to '/etc/zabbix/trap.d/ssl_check_mongo.sh' }
            its(:sha256sum) { should eq '5d71e7243efcaf7068887b33a35fcf499c5aeae5a770e822059b4fe9a5f61517' }
        end
    end

    describe "Services:" do
        describe cron do
            it { should have_entry '* * * * * /etc/zabbix/trap.d/runtrap live >/dev/null 2>&1' }
        end

        describe service('zabbix-agent') do
            it { should be_enabled }
            it { should be_running }
        end

        describe port(10050) do
            it { should be_listening.with('tcp') }
        end
    end
end
