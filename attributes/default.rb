default['zabbix']['database']['backend'] = 'postgresql'
default['zabbix']['database']['repo']['pgdg'] = false
default['zabbix']['webserver']['backend'] = 'apache'
default['zabbix']['webserver']['port'] = '80'
default['zabbix']['php']['use_fpm'] = true
default['zabbix']['php']['timezone'] = 'America/New_York'
default['zabbix']['ssl']['enable'] = false
default['zabbix']['ssl']['port'] = '443'
default['zabbix']['ssl']['cacert'] = nil
default['zabbix']['ssl']['server_cert'] = nil
default['zabbix']['ssl']['server_key'] = nil
default['zabbix']['ssl']['ssl_protocols'] = [ "TLSv1.2" ]
default['zabbix']['ssl']['ssl_ciphers'] = [
  "HIGH",
  "!aNULL",
  "!MD5"
]

default['zabbix']['version'] = "3.0"
default['zabbix']['agent_meta'] = []
if node.run_list?('recipe[zabbix::server]')
    default['zabbix']['agent_servers'] = ['127.0.0.1']
elsif node.run_list?('recipe[zabbix::proxy]')
    default['zabbix']['agent_servers'] = ['127.0.0.1']
else
    default['zabbix']['agent_servers'] = ['127.0.0.1']
end

default['zabbix']['trap_scripts']['live']['add'] = []
default['zabbix']['trap_scripts']['live']['del'] = []
default['zabbix']['trap_scripts']['daily']['add'] = []
default['zabbix']['trap_scripts']['daily']['del'] = []

default['zabbix']['httpd_conf']['https_redirection_enabled'] = true
default['letsencrypt']['enabled'] = false
default['lba_host'] = 'default'
