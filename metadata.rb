name             'zabbix'
maintainer       'Eric Renfro'
maintainer_email 'psi-jack@linux-help.org'
license          'Apache 2.0'
description      'Installs Zabbix Server & Client agents for EL linux distributions'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.5.1'
source_url 'https://git.linux-help.org/Linux-Help/zabbix' if respond_to?(:source_url)
issues_url 'https://git.linux-help.org/Linux-Help/zabbix/issues' if respond_to?(:issues_url)

%w(amazon centos redhat oracle scientific).each do |os|
  supports os
end

recipe 'zabbix', 'Installs zabbix'

depends 'chef-vault', '>= 2.1.1'
depends 'apache2', '>= 3.2.2'
depends 'chef_nginx', '>= 5.0.7'
#depends 'httpd', '>= 0.4.4'
depends 'php', '>= 2.2.0'
depends 'postgresql', '>= 6.0.1'
depends 'yum-ius', '>= 2.1.0'
depends 'yum-zabbix', '>= 1.0.1'
#depends 'apt-zabbix'
#depends 'mysql', '>= 8.2.0'

chef_version '>= 12.1' if respond_to?(:chef_version)
